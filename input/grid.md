---
title: Grid JSON
---

# Grid

The grids object is the primary means of creating page strucutre. Grids is used to create both rows and columns. This is why the `type` property is required.

```json
{
    "type": "row",
    "template": "grid",
    "contents": [
        {
            "type": "column",
            "template": "grid",
            "contents": [
                // Column Contents
            ]
        }
    ]
}
```

## Rows

Rows are how to reserver a section of horizontal space. Rows take up the full width of there containers and will grow to fill their contents. To create a row with the grid template, the type of `row` must be specified. Rows do not have other require properties. Additional pass-through properties like `id` can be used when needed for enhanced page interactions like collapsable sections. 

```json
{
    "type": "row",
    "template": "grid",
    "contents": []
}
```

### Row specific properties

| Properties | Required | Description |
| ---------- | -------- | ----------- |
| `id` | No | Provide a row with a unique ID |


## Columns

Columns are how potions of a row a reserved. The `width` is the required property used to designate how much a column should take up. It is important to note, as screen sizes shrink contents of columns will start to change as well. Once a specific breakpoint is hit, columns will adjust to appear stacked rather than side by side. 

```json
{
    "type": "column",
    "template": "grid",
    "width": "full",
    "contents": []
}
```

### Column specific properties

| Properties | Required | Description |
| ---------- | -------- | ----------- |
| `id` | No | Provide a column with a unique ID |
| `style` | No | Allows for additional style tweaks. Support values include: <ul><li>`align-center` - center align all contents</li><li>`align-right` - right align all contents</li></ul> |
| `width` | Yes | Defines how much space a column takes up inside of the row. Support values include: <ul><li>`half`</li><li>`full`</li></ul> |

## Combinded Example

```json
{
    "type": "row",
    "template": "grid",
    "contents": [
        {
            "type": "column",
            "template": "grid",
            "width": "half"
            "contents": [
                // ... Omitted 
            ]
        },
        {
            "type": "column",
            "template": "grid",
            "width": "half"
            "contents": [
                // ... Omitted 
            ]
        }
    ]
}
```