---
title: Rendering Templates
---

# Rendering & Templates

Empire is based on a client-side rendering system. All empire pages are simple HTML pages that contents are found in a large JSON object typically referred to as "Page JSON." A pages Page JSON is an extensive collection of objects and arrays that contain different types of metadata, configuration, and ASTs needed to render a page accordingly. 

Page JSON can be found in all pages by referencing the global `fwData` object in the browsers developer console. The root properties of the `fwData` are as follows:.


| Property/Key | Data Type | Platform | Description |
| ------------ | --------- | ----------- | ----------- |
| header | AST | Empire/eCore | Header AST |
| footer | AST | eCore | Footer AST |
| page | AST | Empire/eCore | Page AST |
| context | Object | Empire/eCore | Contains page, framework, and component level configurations and metadata. Example: session `keepAlive`, `ui` asset references, and `screen` information |
| menus | Object | Empire/eCore | Contains the main page level menus. Each menu is a directly named sub object like `global` or `system`. The menus the appear below this root object are depends on the platform.  |
| preferences | Object | Empire | Users saved platform specific configurations. Example. tabset pin setting, table preferences |
| notifications | Object | Empire | Platform notifications |

## What an AST is, how its used, what are the common properties

An AST or Abstract Syntax Tree is a collection of objects defining the overall structure of a page. Each node is built from a set of standard and node-specific/template properties. These nodes are paired with the correct corresponding templates, which will render the proper corresponding HTML component or node.

Below are the common AST properties used by the rendering component and how they are used by the render. 

| Property/Key | Data Type | Description |
| ------------ | --------- | ----------- |
| type | String | Type is a string used either to specify a specific mode a template should run in or the actual HTML element that should be rendered when the template property is not specified. |
| template | String | The specific template used to render the current object. When this property is missing, the render will set the property to `universal.` The universal template is a special template that uses an object `type` property as the literal node name for the DOM it would create. If the `type` property is also missing, the render will substitute the current object node with a standard `<div>` element.. |
| contents | Array | Contents define an object node as a branch in the overall structure of the DOM tree. When the render comes across any template node that contains this property, it will loop through the contents children array recursively appending any children results to the current object DOM object. This only happens if the existing object node can properly generate a valid DOM element. |
| attributes | Object | The contents should contain only key-value pair strings that are applied to the root object being rendered. |