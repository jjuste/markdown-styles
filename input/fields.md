---
title: Fields
---

## Basic Button - Row

| Property/Key | Data Type | Description |
| ------------ | --------- | ----------- |
| type | Row |  |
| template | Grid |  |
| contents | Array |  |
| attributes | Object |  |

## Basic Button - Column

| Property/Key | Data Type | Description |
| ------------ | --------- | ----------- |
| type | Column |  |
| template | Grid |  |
| contents | Array |  |
| attributes | Object |  |

## Basic Button - Button

| Property/Key | Data Type | Description |
| ------------ | --------- | ----------- |
| type | Button |  |
| template | Field |  |
| contents | Array |  |


